<!DOCTYPE html>
<html lang="fr">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta charset="UTF-8">
    <title>Mes Parcours</title>
    <link rel="stylesheet" href="src/css/global.min.css">
    <!-- Hotjar Tracking Code for https://isabpcoaching.fr -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1659677,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
</head>
<body>
<?php
require "src/elements/header.php"
?>

<?php
$p = '';
if (isset($_GET['p'])){
    $p = $_GET['p'];
}
?>

<section class="programs__first">
    <div class="dark_overlay"></div>
    <div class="contain">
        <h1>Les Programmes</h1>
        <h3>Trouvez le programme qui vous permettra de passer au niveau supérieur !</h3>
    </div>
</section>

<section class="programs__second">
    <div class="contain">
        <h2>
            Sélectionnez un de mes programmes pour en voir tous les détails !
        </h2>

        <div class="programs">

            <div class="program child">
                <div class="dark_overlay"></div>
                <h3>Programme Être Jeune et Acteur de son parcours</h3>
            </div>

            <div class="program ikigai">
                <div class="dark_overlay"></div>
                <h3>Programme Ikigai</h3>
            </div>

            <div class="program hs">
                <div class="dark_overlay"></div>
                <h3>Programme Hypersensibilité</h3>
            </div>

            <div class="program working">
                <div class="dark_overlay"></div>
                <h3>Programme Bien-Être au Travail</h3>
            </div>

            <div class="program custom">
                <div class="dark_overlay"></div>
                <h3>Accompagnement Personnalisé</h3>
            </div>
        </div>

    </div>

</section>

<section id="ikigai" class="program__third programs__ikigai <?php echo ($p == 'ikigai') ? 'active' : ''?>">
    <div class="contain">
        <div class="title">
            <h2>Programme Ikigai</h2>
            <h4>Trouvez ou Retrouvez votre chemin de vie grace à l'Ikigai</h4>
            <p>Découvrez qui vous êtes et ce qui vous fait vibrer pour donner du sens à vos actions et vos projets. Voyage au coeur de soi !</p>
        </div>
        <div class="what">
            <div class="infos">
                <h3>Qu'est-ce que le programme Ikigai ?</h3>
                <p>L'Ikigai est avant tout, une philosophie de vie, venue du Japon. Considéré comme l'une des raisons de la longévité des habitants de l'île d'Okinawa (qui recense le plus grand nombre de centenaires en bonne santé), trouver son Ikigai permet aux individus de découvrir leur raison d'être profonde, leur joie de vivre.  Il est la synthèse de vos envies, vos valeurs, les besoins de la sociétés et de vos compétences.</p>
                <p>Plus qu'une idée, votre Ikigai est votre passion, votre énergie qui vous permet de vous réveiller et de vous lever chaque matin. Fluctuant selon votre parcours et votre expérience, il révèle votre juste place à cet instant.</p>
                <p>Chercher et trouver son Ikigai vous permet d'être aligné avec vous même chaque jour. Cela se traduit pour une motivation intrinsèque, de l'énergie, une forte estime de soi, une compréhension réelle de votre place dans le monde et un relationnel amélioré.</p>
            </div>
            <div class="img">
                <img src="/src/img/programs/ikigaiimg.png" alt="">

            </div>
        </div>
        <div class="moreInformations">
            <p>Chercher son Ikigaï, c’est s’autoriser à faire une pause, prendre conscience de ce que l’on est et de ce qui est important pour nous, et le verbaliser.</p>
            <h5><b>L’Ikigai se trouve au centre de 4 cercles :  Ce que aimez faire, ce pour quoi vous êtes naturellement doué, ce dont le monde a besoin et ce pour quoi vous pouvez être payé !</b></h5>
            <p>Trouver son Ikigai, c'est savoir faire les choix qui nous correspondent et qui sont vraiment bons pour nous !</p>
            <p>Trouver son Ikigai révèle aussi toutes les options que l'on ne voyait pas encore pour aller vers le mode de vie qui nous correspond, qui nous fait nous épanouir dans notre vie professionnelle ou personnelle...</p>
        </div>
        <div class="whywho">
            <div class="why">
                <h3>Pourquoi faire ce programme ?</h3>
                <ul>
                    <li>Pour mieux vous connaître, prendre le temps de l’introspection nécessaire à tout parcours d’évolution.</li>
                    <li>Découvrir les domaines qui vous font vibrer, les actions à initier, le ou les projets qui vous permettront d’être accompli et épanoui.</li>
                    <li>Pour être éclairé sur vos choix, et adopter ceux qui vous ressemblent.</li>
                    <li>Être en accord avec soi, éviter que vos mots ne se transforment en maux, se sentir à votre place, utile, bien dans votre environnement.</li>
                    <li>Pour assumer vos envies, vos choix, vos rêves et vous interroger sur vos réelles aspirations.</li>
                    <li>Pour dépasser votre  peur du changement et ne plus vous réfugier dans un quotidien qui vous  rassure mais qui n’est pas forcément bon pour vous.</li>
                    <li>Pour retrouver du plaisir à être, à faire...</li>
                </ul>
            </div>
            <div class="who">
                <h3>Ce programme est fait pour vous si:</h3>
                <ul>
                    <li>Vous souhaitez donner ou redonner du sens à votre/vos actions, vos activités, votre emploi, vos choix.</li>
                    <li>Vous n’avez jamais pris le temps de vous occuper de vous et de définir vos réels besoins et envies</li>
                    <li>Vous pensez avoir fait le tour mais vous n’avez aucune idée de ce dans quoi vous pourriez travailler ou quelle activité vous pourriez mettre en place pour retrouver du plaisir.</li>
                    <li>Vous souhaitez être plus en accord avec vous même, et retrouver la joie et le plaisir pour enrichir votre vie</li>
                    <li>Vous ne vous sentez plus à votre place.</li>
                    <li>Vous sentez le besoin d’apporter un changement positif dans votre quotidien mais vous ne savez pas lequel ni dans quel domaine.</li>
                </ul>
            </div>

        </div>
        <div class="reassurance">
            <h3>Ma plus-value:</h3>
            <h4><b>Intervenante spécialisée et formée dans l'accompagnement. Je suis certifiée par le Centre International d'Ikigai</b></h4>
            <p>Un outil complet avec des exercices riches de sens pour vous découvrir et vous révéler</p>
            <p>2 à 3 séances de coaching individuel pour une double lecture et un approfondissement de votre travail de réflexion afin de faciliter la compréhension et vous aider à passer à l'action et / ou des clés pour gérer le stress de cette période de transition</p>
            <div class="cta">
                <h4>Option Découverte : 120 € <br> Option Expérience Positive : 297 € <br> Option Transformation : 750 €</h4>
                <a href="https://subscriptions.isabpcoaching.fr/ikigai" class="call">
                    Voir les options en détail
                </a>
            </div>
        </div>
    </div>
</section>

<section id="hs" class="program__third programs__hs <?php echo ($p == 'hs') ? 'active' : ''?>">
    <div class="contain">
        <div class="title">
            <h2>Programme Hypersensibilité</h2>
            <h4>Votre Hypersensibilité est une qualité, apprenez à la vivre comme une chance !</h4>
            <p>Votre hypersensibilité ne sera plus un fardeau, mais une force : la reconnaître, l’accepter et comprendre comment elle fait de vous un être riche et unique !</p>
        </div>
        <div class="what">
            <div class="infos">
                <h3>Qu'est-ce que le programme Hypersensibilité ?</h3>
                <p>L’hypersensibilité est une caractéristique qui touche 25 % de la population. Il s’agit d’une Sensibilité extrême, exacerbée à la fois Sensorielle (bruit, lumière, odeur, toucher, température, foule…) et Emotionnelle (émotions décuplées, intensité plus forte).</p>
                <p>L'Hypersensibilité s’appuie sur des constats scientifiques et physiologiques qui démontrent que les personnes hypersensibles sont en hyperstimulation, ce qui correspond à une saturation du système nerveux qui est plus sensible et plus actif.</p>
                <p>La Sensibilité est souvent moquée, raillée…et donc perçue comme un ensemble de symptômes, difficultés, faiblesses. Les hypersensibles se sont souvent forgé de nombreuses croyances limitantes de type « Je suis….inadapté, anormal, en décalage, incapable », ce qui engendre une faible confiance en soi et une faible estime de soi.</p>
                <p>Or, offrir un espace d’écoute bienveillante et un questionnement porteur de sens permettent d’identifier, accueillir et autoriser cette hypersensibilité.</p>
            </div>
            <div class="img">
                <img src="/src/img/programs/hypersensibility.png" alt="">

            </div>
        </div>
        <div class="moreInformations">
            <h5><b>L'hypersensibilité est une caractéristique et non  une pathologie. Il est possible de changer son regard et d’en faire une force. Elle recèle des trésors tels qu’une forte intuition, une créativité développée, une empathie hors norme...</b></h5>
            <h5><b>Je suis très sensibilisée aux difficultés engendrées par l’hypersensibilité et les trésors qu’elle renferme...</b></h5>
        </div>
        <div class="whywho">
            <div class="why">
                <h3>Pourquoi faire ce programme ?</h3>
                <ul>
                    <li>Pour être à l’écoute de soi et dépasser vos croyances limitantes.</li>
                    <li>Pour faire de votre spécificité une force.</li>
                    <li>Pour sortir des stratégies inadaptées et ne plus subir la contagion émotionnelle.</li>
                    <li>Pour une pleine et entière expression de soi.</li>
                    <li>Pour identifier vos limites mais surtout vos ressources.</li>
                    <li>Pour identifier votre juste niveau de stimulation et trouver votre équilibre.</li>
                    <li>Pour mieux gérer vos émotions et votre stress.</li>
                    <li>Pour reprendre confiance en vous et développer son estime de soi.</li>
                </ul>
            </div>
            <div class="who">
                <h3>Ce programme est fait pour vous si:</h3>
                <ul>
                    <li>Vous ressentez TOUT plus fort et que cela vous submerge.</li>
                    <li>Vous vous sentez différent, bizarre, avec un sentiment de honte généralisé.</li>
                    <li>Vous avez du mal à vous intégrer car vous pensez que votre hypersensibilité est un défaut, une faiblesse.</li>
                    <li>Vous avez l’impression d’aller sans cesse contre votre vraie nature et cela vous coûte en énergie et en estime de vous-même.</li>
                    <li>Vous étouffez votre personnalité en cherchant sans cesse à plaire aux autres en ayant l’air “comme tout le monde”.</li>
                    <li>Vos émotions sont exacerbées, vous vous sentez souvent blessé, pas à votre place.</li>
                    <li>Vous ne supportez ni le bruit, ni les conflits, ni les films d’horreur et ne savez pas comment le gérer.</li>
                </ul>
            </div>

        </div>
        <div class="reassurance">
            <h3>Ma plus-value:</h3>
            <h4><b>Spécialisée dans l’accompagnement, j'ai réalisé un mémoire de recherche sur l’hypersensibilité, et j’ai pu transformer ma propre hypersensibilité en atout.</b></h4>
            <p>Un accompagnement bienveillant, adapté à vos besoins, à votre rythme</p>
            <p>4 à 10  séances de coaching individuel pour analyser votre demande, définir votre objectif, et vous permettre de passer à  l'action pour mettre en place le changement positif que vous souhaitez voir se réaliser.</p>
            <div class="cta">
                <h4>Option Découverte : 80 € <br> Option Expérience Positive : 297 € <br> Option Transformation : 750 €</h4>
                <a href="https://subscriptions.isabpcoaching.fr/hypersensibility" class="call">
                    Voir les options en détail
                </a>
            </div>
        </div>
    </div>
</section>

<section id="working" class="program__third programs__working <?php echo ($p == 'working') ? 'active' : ''?>">
    <div class="contain">
        <div class="title">
            <h2>Programme Bien-Être au Travail</h2>
            <h4>Faites de votre job une source de satisfaction !</h4>
            <p>En recherche d’emploi, en réflexion sur votre carrière, en lancement d’activité ou juste en quête de bien-être au travail ? Vous êtes au bon endroit !</p>
        </div>
        <div class="what">
            <div class="infos">
                <h3>Qu'est-ce que le programme Bien-être au Travail ?</h3>
                <p>Avec un taux de chômage encore élevé et une nette augmentation du nombre de créations d’activité, nombreux sont ceux qui éprouvent des difficultés pour vivre sereinement de leur travail.</p>
                <p>Les personnes qui sont en recherche d’emploi ressentent vite le poids du regard souvent culpabilisant et réprobateur de la société sur leur soit disant “inactivité”, celles qui se lancent celui des peurs et mises en garde  liées au risque de l’échec et de la faillite et pour finir rares sont les personnes en poste qui se disent  satisfaites de leur travail.</p>
                <p>Or, c’est bien là que nous passons la majorité de notre temps. Mais la peur du chômage, la peur du changement, la croyance selon laquelle nous n’avons aucune marge de manoeuvre, ce que l’on croit que les autres attendent de nous et parfois l’appréhension du regard des autres nous empêche de nous mettre en action pour améliorer ce qui peut l’être ou prendre les décisions qui sont bonnes pour nous.</p>
                <p>Il arrive aussi que l’envie soit là mais sans savoir comment s’y prendre. Le fameux « On sait ce qu’on perd mais on ne sait pas ce qu’on gagne » est terriblement…paralysant ! Et pourtant, il suffit parfois de se poser les bonnes questions, de procéder à quelques modifications, d’oser faire la bonne demande ou la bonne proposition...</p>
                <p>Ce programme vous propose d’identifier vos atouts, vos forces, vos envies pour  vous mettre en action ! Définir un objectif précis pour (re)trouver du sens à ce qu’on fait, se sentir compétent, utile, reconnu et surtout bien dans son job ! Agir et ne plus subir, ça vous tente ?</p>
            </div>
            <div class="img">
                <img src="/src/img/programs/working.png" alt="">

            </div>
        </div>
        <div class="moreInformations">
            <h5><b>Se sentir bien au travail c’est possible ! La première personne responsable de votre bien-être c’est vous ! Vous êtes intelligent, plein de qualités et vous pouvez agir pour améliorer votre quotidien. Le tout est d’identifier ce qui est important pour vous, vos ressources, la manière de les mobiliser et de faire le premier pas !</b></h5>
        </div>
        <div class="whywho">
            <div class="why">
                <h3>Pourquoi faire ce programme ?</h3>
                <ul>
                    <li>Pour vous donner toutes les chances de trouver un emploi.</li>
                    <li>Pour réfléchir à votre carrière professionnelle.</li>
                    <li>Pour mieux gérer votre stress.</li>
                    <li>Pour améliorer votre relation aux autres.</li>
                    <li>Pour prévenir le burn-out.</li>
                    <li>Pour équilibrer vie personnelle et professionnelle.</li>
                    <li>Renforcer votre motivation.</li>
                    <li>Mieux gérer votre temps.</li>
                    <li>Améliorer votre confiance en soi.</li>
                </ul>
            </div>
            <div class="who">
                <h3>Ce programme est fait pour vous si:</h3>
                <ul>
                    <li>Vous recherchez un emploi et avez besoin de vous faire accompagner pour reprendre confiance en vous, apprendre à valoriser votre image et votre parcours, préparer les entretiens de recrutement, gérer votre stress...</li>
                    <li>Vous avez perdu le sens que vous donniez à votre travail.</li>
                    <li>Vous sous sentez en décalage avec votre métier et/ou votre entreprise.</li>
                    <li>Vous souhaitez réfléchir à de nouvelles pistes de travail ou d’évolution professionnelle mais vous ne savez pas par ou commencer.</li>
                    <li>Vous vous lancez à votre compte mais avez du mal à lever les derniers freins.</li>
                    <li>Vous prenez un nouveau poste et avez peur de ne pas être à la hauteur.</li>
                    <li>Vous êtes dirigeant, rencontrez des difficultés mais ne savez pas à qui vous adresser pour trouver des solutions en toute confidentialité.</li>
                </ul>
            </div>

        </div>
        <div class="reassurance">
            <h3>Ma plus-value:</h3>
            <h4><b>Professionnelle de l’insertion et spécialisée dans l'accompagnement individuel et collectif (ancienne dirigeante d’une structure régionale), j'ai à coeur de favoriser qualité de vie au travail</b></h4>
            <p>Un accompagnement adapté à vos besoins, à votre rythme</p>
            <p>4 à 10  séances de coaching individuel pour analyser votre demande, définir votre objectif, et vous permettre de passer à  l'action pour mettre en place le changement positif que vous souhaitez voir se réaliser.</p>
            <div class="cta">
                <h4>Option Découverte : 150 € <br> Option Expérience Positive : 297 € <br> Option Transformation : 750 €</h4>
                <a href="https://subscriptions.isabpcoaching.fr/qvt" class="call">
                    Voir les options en détail
                </a>
            </div>
        </div>
    </div>
</section>

<section id="child" class="program__third programs__child <?php echo ($p == 'child') ? 'active' : ''?>">
    <div class="contain">

        <div class="title">
            <h2>Programme Être Jeune et Acteur de son parcours</h2>
            <h4>Être jeune et Acteur de son parcours : des clés pour faire ses choix et construire son avenir</h4>
            <p>Découvrez votre potentiel, retrouvez la motivation nécessaire pour prendre en main votre avenir et décider seul de vos choix.</p>
        </div>

        <div class="what">
            <div class="infos">
                <h3>Qu'est-ce que le programme Être Jeune et Acteur de son parcours ?</h3>
                <p>La question qu’entend le plus souvent un jeune est « Que veux-tu faire plus tard ? » Que répondre à cette question quand on ne se connait pas soi-même ?</p>
                <p>Être jeune, c’est avoir l’avenir devant soi et devoir choisir ce que l’on va en faire, c’est l’heure des engagements. Être jeune c’est devoir faire face à ses émotions, gérer le stress des contrôles, des examens, du premier stage, du premier oral, du premier entretien, du premier job… Que d’expériences nouvelles sans y être vraiment préparé ! Sans vraiment maitriser (ni même comprendre) ce qui se passe en soi.</p>
                <p>Coincé entre ses désirs, ses envies, ses passions, ses rêves et les attentes de son entourage, de sa famille, ses amis, c’est souvent difficile pour une jeune femme ou un jeune homme de s’y retrouver, de se motiver et de se sentir bien dans sa peau.</p>
                <p>Pour découvrir un métier, l’idéal est de questionner celui qui l’exerce, pour découvrir ce que l’on veut faire, c’est soi qu’il faut questionner. Qui suis-je ? Qu’est ce qui m’anime ? Qu’est ce qui me donne du plaisir ? Pourquoi suis-je doué ? Quelles sont mes compétences ? Qu’ai-je envie de créer, réaliser, apporter ? Qui ai-je envie de devenir ?</p>
                <p>Mais comment être à la fois celui qui pose la question et celui qui y répond ? C’est pourtant cela que l’on demande aux élèves et étudiants lorsqu’ils doivent choisir leurs options, leurs études, leur orientation. Sans ces réponses, sans ces informations, comment construire son parcours ? Retrouver de la motivation et faire face au stress ?</p>
                <p>C’est précisément à ces questions que le coaching propose de répondre grâce à une écoute bienveillante, un questionnement adapté, des outils spécifiques, la définition d’un objectif précis, et la recherche conjointe d'options. Une fois ces éléments en main la recherche documentaire pour arrêter ses choix devient alors plus fluide, plus facile et orientée.</p>
            </div>

            <div class="infos parents">
                <h3>Message aux parents :</h3>
                <p><b>Vous souhaitez le meilleur pour vos enfants ? Vous souhaitez les aider à identifier leurs atouts et ce qui leur permettra de s’épanouir ? Les
                    aider à faire leurs propres choix, et faire les premiers pas vers un avenir qui leur ressemble ? Alors vous êtes au bon endroit !</b>
                </p>
                <p>Les ressources, ils les ont, seulement ils n’en ont pas toujours conscience et souvent, ne savent pas les mobiliser
                    L’envie, ils l’ont, encore faut-il qu’ils définissent leur objectif pour avoir envie de l’atteindre et surtout le bénéfice qu’ils en retireront
                    L’énergie pour réussir, ils l’ont, mais à condition d’avoir pu identifier ce qui les anime, leur donne du plaisir, pour donner du sens à l’action
                </p>
                <p>
                    Les étapes nécessaires sont : la connaissance de soi, la définition d’objectifs, la mise
                    en œuvre d’un plan d’action fait de petits pas pour se rapprocher du sommet qui leur semble
                    parfois impossible à atteindre. Et enfin la projection vers un avenir qui répond à leurs envies,
                    leurs gouts, et surtout ce qu’ils sont. C’est à travers l’introspection, la valorisation des
                    réussites, et la confiance qu’il leur sera possible de trouver le sens des efforts à fournir pour
                    être à la hauteur de leur ambition. C’est cette démarche que propose le coaching pour les
                    rendre acteur de leur avenir !
                </p>
                <p>
                    Et enfin, avant toute démarche, il est essentiel de savoir si <b>votre enfant est vraiment motivé</b>, s’il manifeste la volonté de s’impliquer dans un coaching. Car en effet, l’objectif
                    premier est bien de l’accompagner dans cette réflexion mais en aucun cas le coach ne devra
                    décider pour lui. C’est lui qui part à la découverte de son potentiel et de ses solutions !
                </p>

            </div>
        </div>
        <div class="moreInformations">
            <h3>Quand et pour quel bénéfice ?</h3>
            <p>Pour les situations scolaires ou universitaires qui posent problème, avec un enjeu important (choix à faire, démotivation, absentéisme, angoisse avant les contrôles/examens), le coaching permet à l’élève ou l’étudiant, en quelques séances, de trouver lui-même les solutions aux problèmes qui gênent son orientation, sa réussite ou son épanouissement.</p>
        </div>
        <div class="whywho">
            <div class="why">
                <h3>Pourquoi faire ce programme ?</h3>
                <ul>
                    <li>Choisir un métier, construire un projet professionnel, s’orienter.</li>
                    <li>Identifier ses aptitudes, ses gouts, ses qualités.</li>
                    <li>Gérer son stress / son temps.</li>
                    <li>Améliorer sa relation aux autres.</li>
                    <li>Améliorer sa confiance en soi.</li>
                    <li>Explorer de nouveaux horizons.</li>
                    <li>Se donner toutes les chances de trouver un 1er emploi (CV, lettre de motivation, préparer l’entretien d’embauche).</li>
                </ul>
            </div>
            <div class="who">
                <h3>Ce programme est fait pour vous si:</h3>
                <ul>
                    <li>Vous vous questionnez sur votre Orientation</li>
                    <li>Vous souhaitez construire votre projet professionnel</li>
                    <li>Vous souhaitez vous réorienter (erreur de filière, de FAC…)</li>
                    <li>Vous préparez un examen, un concours, un oral</li>
                    <li>Vous vous sentez démotivé</li>
                    <li>Vous souhaitez améliorer votre confiance en vous</li>
                    <li>Vous voulez vous préparer aux entretiens d’embauche (job étudiant, job d’été, 1er job)</li>
                </ul>
            </div>

        </div>
        <div class="reassurance">
            <h3>Ma plus-value:</h3>
            <h4><b>J’ai œuvré 23 ans dans un réseau d’accompagnement des jeunes de 16 à 25 ans.</b></h4>
            <p>J’interviens au CNAM sur la connaissance du public jeune et les spécificités de leur accompagnement vers l’autonomie. Je dispose d’outils spécifiques d’accompagnement (dont des outils numériques pour identifier les compétences transversales)</p>
            <p>4 à 10  séances de coaching individuel pour analyser votre demande, définir votre objectif, et vous permettre de passer à  l'action pour mettre en place le changement positif que vous souhaitez voir se réaliser.</p>
            <div class="cta">
                <h4>Option Découverte : 150 € <br> Option Expérience Positive : 297 € <br> Option Transformation : 750 €</h4>
                <a href="https://subscriptions.isabpcoaching.fr/youth" class="call">
                    Voir les options en détail
                </a>
            </div>
        </div>
    </div>
</section>

<section id="custom" class="program__third programs__custom <?php echo ($p == 'custom') ? 'active' : ''?>">
    <div class="contain">
        <div class="title">
            <h2>Accompagnement Personnalisé</h2>
            <h4>Un Accompagnement Personnalisé, pour atteindre la destination de vos rêves !</h4>
            <p>Vous souhaitez aller plus loin dans votre vie, briser vos croyances limitantes et avancer vers vos objectifs, rencontrez moi pour créer un parcours rien que pour vous !</p>
        </div>
        <div class="what">
            <div class="infos">
                <h3>Qu'est-ce que le programme Personnalisé ?</h3>
                <p>Vous souhaitez apporter du changement dans votre vie ? Ou vous êtes confronté à un changement récent ou à venir (milieu de vie, départ à la retraite…).Vous souhaitez mieux vous connaître pour avancer, évoluer, améliorer l‘un de vos domaines de vie ? Vous souhaitez identifier vos ressources, vos talents, les valeurs sur lesquelles vous appuyer, et levez les obstacles pour atteindre ce que vous désirez de manière sereine !</p>
                <p>« Suis-je bien à ma place ? », « Que pourrais-je améliorer et comment ? », “Quelles solutions pour être mieux”, « Quel plan d’action mettre en place ? »</p>
                <p>Pas de promesse en l’air, la vie idéale et sans effort n’existe que dans les contes,  je n’ai (malheureusement) pas de baguette magique, et votre bien-être dépend d’abord de vous mais je peux vous accompagner sur votre chemin, vous aider à reconnaître les ressources qui  sont les vôtres, celles que vous n’osez pas déployer, celles que vous ne voyez pas ou plus, à identifier ce qui vous freine pour lever ou contourner les obstacles, à découvrir que vous avez en vous toutes les clés pour réussir vos projets, à comprendre que la plus belle chose qui puisse vous arriver dans la vie, c’est d’être VOUS, pleinement et sereinement !</p>
                <p>Devenez acteur de votre bien-être, acteur de votre vie ! En coaching, l’expert c’est vous !</p>
                <p>Le coaching est une démarche auto apprenante, il vise l’évolution ou le développement pérenne par la mise en œuvre de ressources intrinsèques !</p>
            </div>
            <div class="img">
                <img src="/src/img/programs/custom.jpg" alt="">

            </div>
        </div>
        <div class="moreInformations">
            <p>Oui vous pouvez agir ! Oui, vous êtes intelligent ! Oui vous possédez un, voire plusieurs talents !</p>
            <h5><b>Oui le changement est possible ! Oui les solutions, vos solutions, existent !</b></h5>
            <p>Oui vous êtes formidable, parce que nous sommes tous des êtres uniques, riches de notre humanité dotés de ressources qui ne demandent qu’à être exploitées !</p>
            <p>Oui vous pouvez le faire, il suffit d’oser le tout premier petit pas et  passer à l’action en douceur, à votre rythme, en toute sécurité : parlons en !</p>
        </div>
        <div class="whywho">
            <div class="why">
                <h3>Pourquoi faire ce programme ?</h3>
                <ul>
                    <li>Apprendre à vous connaître et donner ou retrouver votre joie de vivre .</li>
                    <li>Trouver vos propres solutions, les plus efficientes possible, pour résoudre votre problématique et  vous défaire de ce qui vous encombre et que vous voulez changer.</li>
                    <li>Améliorer vos  relations (professionnelles, familiales, amicales...).</li>
                    <li>Prendre conscience de vos ressources pour mieux les mobiliser.</li>
                    <li>Identifier les obstacles et la ou les stratégies possibles.</li>
                    <li>Passer à l’action, chaque petit pas vous rapproche de votre objectif, l’important c’est d’avancer !</li>
                    <li>Pour développer vos performances et atteindre vos objectifs.</li>
                    <li>Mieux gérer votre stress.</li>
                </ul>
            </div>
            <div class="who">
                <h3>Ce programme est fait pour vous si:</h3>
                <ul>
                    <li>Vous êtes confronté à un obstacle que vous n’arrivez pas à dépasser seul.</li>
                    <li>Vous êtes confronté à un changement de vie (déménagement, départ à la retraite...)</li>
                    <li>Vous avez des choix à faire (orientation...)</li>
                    <li>Vous souhaitez retrouver un équilibre vie professionnelle / vie personnelle.</li>
                    <li>Vous voulez arrêter de procrastiner.</li>
                    <li>Vous souhaitez améliorer votre communication et vos relations.</li>
                    <li>Vous souhaitez vous épanouir.</li>
                    <li>Vous voulez renforcer votre confiance en vous.</li>
                    <li>
                        Vous vous posez des questions qui restent sans réponse ? Vous êtes disposé(e) à trouver les réponses grâce à un travail d’introspection et de mise en action ? Alors oui, le coaching est fait pour vous !
                    </li>
                </ul>
            </div>

        </div>
        <div class="reassurance">
            <h3>Ma plus-value:</h3>
            <h4><b>20 ans d’expérience dans le domaine de l’Accompagnement individuel et collectif. Psychosociologue de formation (Master 2), j'ai occupé un poste de direction de structure régionale, et je suis certifiée Coach Consultante Professionnelle (RNCP niveau 1, BAC+5)</b></h4>
            <p>Un accompagnement adapté à vos besoins, à votre rythme</p>
            <p>4 à 10  séances de coaching individuel pour analyser votre demande, définir votre objectif, et vous permettre de passer à  l'action pour mettre en place le changement positif que vous souhaitez voir se réaliser.</p>
            <p>Oui ce travail de connaissance de soi, de mise en place de stratégies, d’élaboration d’un plan d’action, de définition de votre objectif… vous pourriez le faire seul, mais s’il est un diction qui résonne à chaque fois que je l’entend, c’est « Seul on va plus vite mais à deux on va plus loin ! »</p>
            <div class="cta">
                <h4>Option Découverte : 80 € <br> Option Expérience Positive : 297 € <br> Option Transformation : 750 €</h4>
                <a href="https://subscriptions.isabpcoaching.fr/custom" class="call">
                    Voir les options en détail
                </a>
            </div>
        </div>
    </div>
</section>

<?php
require "src/elements/footer.php"
?>
<script src="/src/js/programstoggle.js"></script>
<script src="/src/js/scrollTo.js"></script>
</body>
</html>