<?php

session_start()

?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta charset="UTF-8">
    <title>Contact</title>
    <link rel="stylesheet" href="src/css/global.min.css">
</head>
<body>
<?php
require "src/elements/header.php"
?>



<div class="contactform">

    <div class="contain">
        <?php if (array_key_exists('errors', $_SESSION)): ?>
            <div class="alert">
                <?= implode('<br>', $_SESSION['errors']); ?>
            </div>
            <?php unset($_SESSION['errors']); endif; ?>

        <?php if (array_key_exists('success', $_SESSION)): ?>
            <div class="alert">
                Votre mail nous est bien parvenu.
            </div>
            <?php unset($_SESSION['success']); endif; ?>
        <h1>Me contacter</h1>
        <form action="src/elements/mail.php" method="post">

            <div class="form fxs">
                <label for="inputname">Votre nom</label>
                <input required type="text" name="name" id="inputname" placeholder="Votre Nom" value="<?= isset($_SESSION['inputs']['name']) ? $_SESSION['inputs']['name'] : ''; ?>">
            </div>

            <div class="form fxs">
                <label for="inputemail">Votre email</label>
                <input required type="email" name="email" id="inputemail" placeholder="Votre Adresse Mail" value="<?= isset($_SESSION['inputs']['email']) ? $_SESSION['inputs']['email'] : ''; ?>">
            </div>


            <div class="form freg">
                <label for="inputmessage">Votre message</label>
                <textarea required name="message" id="inputmessage" cols="30" rows="10"> <?= isset($_SESSION['inputs']['message']) ? $_SESSION['inputs']['message'] : ''; ?> </textarea>
            </div>

            <button type="submit" class="btn">Envoyer</button>
        </form>
    </div>



</div>

<?php
require "src/elements/footer.php"
?>


</body>
</html>

<?php
unset($_SESSION['inputs']);
unset($_SESSION['success']);
unset($_SESSION['errors']);
?>