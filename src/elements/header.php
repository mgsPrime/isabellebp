<div class="header">
    <img src="./src/img/logo.png" class="header__logo">
    <menu class="header__menu">
        <a href="/index.php" class="header__a">Accueil</a>
        <a href="/about.php" class="header__a">A propos de moi</a>
        <a href="/coaching.php" class="header__a">Le Coaching</a>
        <a href="/programs.php" class="header__a">Les Programmes</a>
        <a href="/contact.php" class="header__a">Contact</a>
    </menu>
    <a href="#" class="header__burger">
        <div class="pix"></div>
        <div class="pix"></div>
        <div class="pix"></div>
        <div class="pix"></div>
        <div class="pix"></div>
        <div class="pix"></div>
        <div class="pix"></div>
        <div class="pix"></div>
        <div class="pix"></div>
    </a>
    <ol class="header__menu2">
        <li class="cuboidmenu">
            <a href="/index.php">
                <img src="/src/img/icons/methode.png" alt="">
            </a>
            <p>
                <a href="/index.php">Accueil</a>
            </p>

        </li>
        <li class="cuboidmenu">
            <a href="/about.php">
                <img src="/src/img/icons/who.png" alt="">
            </a>
            <p>
                <a href="/about.php">Qui suis-je ?</a>
            </p>
        </li>
        <li class="cuboidmenu">
            <a href="/coaching.php">
                <img src="/src/img/icons/coaching.png" alt="">
            </a>
            <p>
                <a href="/coaching.php">Le Coaching</a>
            </p>

        </li>
        <li class="cuboidmenu">
            <a href="/programs.php">
                <img src="/src/img/icons/hypersensibility.png" alt="">
            </a>
            <p>
                <a href="/programs.php">Les Parcours</a>
            </p>

        </li>
        <li class="cuboidmenu">
            <a href="/contact.php">
                <img src="/src/img/icons/contact.png" alt="">
            </a>
            <p>
                <a href="/contact.php">Contact</a>
            </p>

        </li>

    </ol>

</div>
<script type="text/javascript" src="/src/js/toggle.js"></script>