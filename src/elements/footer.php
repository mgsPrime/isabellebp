<div class="footer">
    <div class="contain">
        <img src="/src/img/logo.png" alt="">
        <div class="about">
            <h4>A Propos</h4>
            <ul>
                <li><a href="/about.php#second">Mon Histoire</a></li>
                <li><a href="/coaching.php">Le Coaching, c'est quoi ?</a></li>
                <li><a href="/about.php#third">Mes valeurs, ma méthode, ma vision</a></li>
            </ul>
        </div>

        <div class="parcours">
            <h4>Mes Programmes</h4>
            <ul>
                <li><a href="/programs.php?p=ikigai">L'Ikigai</a></li>
                <li><a href="/programs.php?p=hs">L'Hypersensibilité</a></li>
                <li><a href="/programs.php?p=working">Le Bien-être au Travail</a></li>
                <li><a href="/programs.php?p=child">Être jeune et acteur de son parcours</a></li>
                <li><a href="/programs.php?p=custom">Accompagnement Personnalisé</a></li>
            </ul>

        </div>

        <div class="contactto">
            <h4>Me Contacter</h4>
            <ul>
                <li><b>Mail :</b> contact@isabpcoaching.fr</li>
                <li><b>Téléphone :</b> 07 54 37 40 71</li>
                <li><a href="https://fr.linkedin.com/in/isabelle-biarnes-poulliat-32a66412"><b>Mon LinkedIn</b></a></li>
                <li><a href="https://www.facebook.com/isabpcoaching/"><b>Mon Facebook</b></a></li>
            </ul>


        </div>

    </div>
    <div class="copyright">
        <p><b>© 2020 Isabelle Biarnes Poulliat - Conception et réalisation : Killian Di Vincenzo - Web & Graphism</b></p>
    </div>
</div>