let ikigaiButton = document.querySelector('.ikigai');
let hsButton = document.querySelector('.hs');
let workingButton = document.querySelector('.working');
let customButton = document.querySelector('.custom');
let childButton = document.querySelector('.child');

let ikigaiSection = document.querySelector('.programs__ikigai');
let hsSection = document.querySelector('.programs__hs');
let workingSection = document.querySelector('.programs__working');
let customSection = document.querySelector('.programs__custom');
let childSection = document.querySelector('.programs__child');

document.querySelectorAll('.program').forEach(q => {
    q.addEventListener('click', function(){

        var qs = document.querySelector('.programs__' + this.className.substr(8));
        qs.classList.toggle('active');
        switch(qs.classList[1]){
            case 'programs__ikigai':
                workingSection.classList.remove("active");
                hsSection.classList.remove("active");
                customSection.classList.remove("active");
                childSection.classList.remove("active");
                break;
            case 'programs__hs':
                workingSection.classList.remove("active");
                ikigaiSection.classList.remove("active");
                customSection.classList.remove("active");
                childSection.classList.remove("active");
                break;
            case 'programs__child':
                hsSection.classList.remove("active");
                ikigaiSection.classList.remove("active");
                customSection.classList.remove("active");
                workingSection.classList.remove("active");
                break;
            case 'programs__working':
                hsSection.classList.remove("active");
                ikigaiSection.classList.remove("active");
                customSection.classList.remove("active");
                childSection.classList.remove("active");
                break;
            case 'programs__custom':
                workingSection.classList.remove("active");
                ikigaiSection.classList.remove("active");
                hsSection.classList.remove("active");
                childSection.classList.remove("active");
                break;
            default:
                console.log('bug');
        }
        qs.scrollIntoView({behavior: 'smooth'})
    });
});


