<!DOCTYPE html>
<html lang="fr">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta charset="UTF-8">
    <title>Isabelle Biarnes-Poulliat</title>
    <meta name="description" content="Vous épanouir et atteindre vos objectifs">
    <link rel="stylesheet" href="src/css/global.min.css">
    <!-- Hotjar Tracking Code for https://isabpcoaching.fr -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1659677,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
</head>
<body>

    <?php
        require "src/elements/header.php"
    ?>

    <section class="home__first">
        <div class="container">
            <div class="wrapper">
                <div class="home__first_lifepath">
                    <h1>Vous épanouir et atteindre vos objectifs</h1>
                    <p class="subtitle">Devenez acteur de votre bien-être, acteur de votre vie !</p>
                    <a href="/programs.php">Débuter l'Aventure</a>
                    <span></span>
                    <p class="baseline">Identifiez vos ressources, vos talents, les valeurs sur lesquelles vous appuyer, et levez les obstacles pour atteindre ce que vous désirez de manière sereine</p>
                </div>
                <img src="src/img/illustration1.png" alt="">
            </div>
        </div>
    </section>

    <section class="home__third">
        <div class="contain">
            <h2>
                Les Programmes
            </h2>

            <div class="program child">
                <div class="dark_overlay"></div>
                <div class="infos">
                    <h3>
                        Être jeune et acteur de son parcours
                    </h3>
                    <p>
                        <b>Découvrez votre potentiel, retrouvez la motivation nécessaire pour prendre en main votre avenir et décider seul de vos choix.</b>
                    </p>
                    <a href="/programs.php?p=child">En savoir plus</a>

                </div>
                <div class="img child"></div>
            </div>

            <div class="program ikigai">
                <div class="infos">
                    <h3>
                        Trouvez ou Retrouvez votre chemin de vie grace à l'Ikigaï
                    </h3>
                    <p>
                        <b>Découvrez qui vous êtes et ce qui vous fait vibrer pour donner du sens à vos actions et vos projets. Voyage au coeur de soi !</b>
                    </p>
                    <a href="/programs.php?p=ikigai">En savoir plus</a>

                </div>
                <div class="dark_overlay"></div>
                <div class="img ikigai"></div>
            </div>

            <div class="program hs">
                <div class="dark_overlay"></div>
                <div class="infos">
                    <h3>
                        Votre Hypersensibilité est une qualité, apprenez à la vivre comme une chance !
                    </h3>
                    <p>
                        <b>Votre hypersensibilité ne sera plus un fardeau, mais une force : la reconnaître, l’accepter et comprendre comment elle fait de vous un être riche et unique !</b>
                    </p>
                    <a href="/programs.php?p=hs">En savoir plus</a>

                </div>
                <div class="img hs"></div>
            </div>

            <div class="program working">
                <div class="dark_overlay"></div>
                <div class="infos">
                    <h3>
                        Faites de votre job une source de satisfaction !
                    </h3>
                    <p>
                        <b>En recherche d’emploi, en réflexion sur votre carrière, en lancement d’activité ou juste en quête de bien-être au travail ? Vous êtes au bon endroit !</b>
                    </p>
                    <a href="/programs.php?p=working">En savoir plus</a>

                </div>
                <div class="img working"></div>
            </div>

            <div class="pcustom">
                <div class="img"></div>

                <div class="infos">
                    <h3>Un Programme Personnalisé, pour vous rapprocher de vos rêves !</h3>

                    <p><b>Vous souhaitez apporter un changement positif dans votre vie, briser vos croyances limitantes et avancer vers vos objectifs, rencontrons-nous pour créer un programme qui vous ressemble !</b></p>
                    <a href="/programs.php?p=custom">En savoir plus</a>
                </div>

            </div>


        </div>
    </section>

    <section class="home__second">
        <div class="contain">
            <div class="story">
                <h2>Isabelle Biarnes-Poulliat</h2>
                <p>
                    Je suis Coach en Développement Personnel et Professionnel. Ma conviction : si chacun trouve sa place, est en accord avec son être et peut exprimer son identité pleine et entière, alors le monde sera meilleur.
                </p>
                <p>
                    Ce qui me motive, c’est accompagner chaque individu à être acteur de sa vie, à donner ou redonner du sens à ses actions, ses projets, pour être en harmonie avec ce qu’il est et avec son environnement.
                </p>
                <p>
                    Rejoignez moi si vous être prêt à partir sur la route de l’épanouissement , du bien-être et du changement positif, afin d’atteindre vos objectifs, et vous rapprocher de vos rêves !
                </p>
                <a href="/about.php">Plus à propos d'Isabelle</a>
            </div>
            <img src="/src/img/isabp.jpg" alt="">
        </div>
    </section>

    <section class="home__fourth">
        <div class="container">
            <div class="title">
                <h3>Le Coaching, c'est quoi ?</h3>
            </div>

            <div class="wrapper">
                <div class="contain">
                    <p>Etre accompagné par un coach c’est se donner toutes les chances d’atteindre son ou ses objectif(s) pour une vie meilleure. Par son art du questionnent et sa bienveillance, un coach vous permet de trouver le chemin de l’autonomie afin définir vos priorités, améliorer vos relations, retrouver un équilibre, évoluer, grandir, vous orienter, vous épanouir… Il suffit d’oser le tout premier pas et passer à l’action en douceur, à votre rythme, en toute sécurité sur la route de la réussite ! Devenez acteur de votre bien-être, acteur de votre vie ! En coaching, l’expert c’est vous !</p>
                    <a href="/coaching.php">En savoir plus</a>
                </div>

                <div class="faq">
                    <div class="q">
                        <p class="qtitle">
                            Que permet le coaching ?
                        </p>
                        <div class="subq">
                            <p>
                                Le coaching, c’est accompagner une personne ou un collectif à atteindre un objectif personnel. Il permet : d’apprendre à se connaître, de prendre conscience de ses ressources pour mieux les mobiliser, de lever les obstacles, d’identifier la ou les stratégies possibles, de passer à l’action et d’élargir son champ des possibles !
                            </p>
                        </div>
                    </div>
                    <div class="q">
                        <p class="qtitle">
                            Le coaching permet-il une réussite rapide et sans effort ?
                        </p>
                        <div class="subq">
                            <p>Pas de promesse en l’air, la vie idéale et sans effort n’existe que dans les contes. Je n’ai (malheureusement) pas de baguette magique ! Votre bien-être dépend d’abord de vous mais je peux vous accompagner sur votre chemin, vous aider à reconnaitre les ressources qui sont les vôtres, à identifier ce qui vous freine pour lever ou contourner les obstacles, et à découvrir que vous avez en vous toutes les clés pour réussir vos projets.</p>
                        </div>
                    </div>
                    <div class="q">
                        <p class="qtitle">
                            Le coaching est-il semblable à une thérapie ?
                        </p>
                        <div class="subq">
                            <p>Le coaching n’est pas une thérapie : il ne s’agit pas de guérir mais d’accompagner le développement et l’évolution pérenne. C’est la réalisation de votre potentiel qui est visé, l’affirmation de votre identité et la capacité à vous situer et vous mouvoir dans vos relations aux autres et au monde.</p>
                        </div>
                    </div>
                    <div class="q">
                        <p class="qtitle">
                            Comment savoir si le coaching est fait pour moi ?
                        </p>
                        <div class="subq">
                            <p>
                                Vous avez envie de redonner du sens à votre vie professionnelle et/ou personnelle ? Vous avez envie de retrouver votre motivation ? Vous vous posez des questions qui restent sans réponse ? Vous êtes disposé(e) à trouver les réponses grâce à un travail d’introspection et de mise en action ? Alors oui, c’est fait pour vous !
                            </p>
                        </div>
                    </div>
                    <div class="q">
                        <p class="qtitle">
                            Il y a-t-il des rencontres en présentiel ?
                        </p>
                        <div class="subq">
                            <p>
                                Les séances peuvent se dérouler soit à distance par Skype ou par téléphone soit en présentiel en fonction de vos besoins et de nos possibilités respectives (lieu neutre, ou à domicile si résident métropole bordelaise)
                            </p>
                        </div>
                    </div>
                </div>

                </div>
            </div>
        </div>


    </section>

    <section class="home__fifth testimonial-section">
        <div class="inner-width">
            <h1>Témoignages</h1>
            <div class="border"></div>
            <div class="slides">

                <div class="testimonial">
                    <div class="test-info">
                        <img src="/src/img/test/s_letter.png" alt="" class="test-pic">
                        <div class="test-name">
                            <span>Sophie</span>
                            Marmande
                        </div>
                    </div>
                    <p>
                        « Engagée, motivée, motivante, bienveillante : les adjectifs sont nombreux ! J'ai eu du plaisir à
                        suivre nos séances et vous m'avez donné des outils qui me sont utiles tous les jours ! Je vous
                        remercie pour votre accompagnement et vous souhaite une belle continuation ! »
                    </p>
                </div>

                <div class="testimonial">
                    <div class="test-info">
                        <img src="/src/img/test/s_letter.png" alt="" class="test-pic">
                        <div class="test-name">
                            <span>Séverine</span>
                            St Médard
                        </div>
                    </div>
                    <p>
                        « J'ai eu le plaisir d'être coachée par Isabelle Biarnes dans le cadre du développement de mon
                        entreprise. Très professionnelle, à l'écoute, j'ai pu grâce à elle me poser les bonnes questions pour
                        trouver le meilleur chemin pour moi. Je vous recommande vivement de travailler avec elle, en plus
                        de son professionnalisme, ses qualités humaines et émotionnelles font une vraie différence. »
                    </p>
                </div>

                <div class="testimonial">
                    <div class="test-info">
                        <img src="/src/img/test/k_letter.png" alt="" class="test-pic">
                        <div class="test-name">
                            <span>Killian</span>
                            St Aubin
                        </div>
                    </div>
                    <p>
                        « Isabelle est l’une des plus remarquables personnes que j’ai rencontré ! J’étais un jeune lycéen qui
                        n’était pas adapté au système scolaire classique, et je ne le suis toujours pas pour être honnête…
                        Mais Isabelle m’a permis de dépasser mes déceptions, mes colères, mes peurs, pour en faire des
                        forces et réveiller le créatif, le visionnaire et le guerrier qui sommeillaient en moi ! Elle m’a donné
                        les clés pour redévelopper ma confiance en moi et me créer un mode de vie qui me correspond
                        davantage ! C’est un plaisir pour moi de connaître Isabelle et de vous la recommander : Son
                        énergie et sa bienveillance seront vous faire passer au niveau supérieur ! »
                    </p>
                </div>


            </div>
        </div>
    </section>

    <?php
    require "src/elements/footer.php"
    ?>


<script type="text/javascript" src="/src/js/qbutton.js"></script>

</body>
</html>