<!DOCTYPE html>
<html lang="fr">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta charset="UTF-8">
    <title>Coaching</title>
    <link rel="stylesheet" href="src/css/global.min.css">
    <!-- Hotjar Tracking Code for https://isabpcoaching.fr -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1659677,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
</head>
<body>

<?php
require "src/elements/header.php"
?>

<section class="coaching__first">
    <div class="dark_overlay"></div>
    <div class="contain">
        <h1>Le Coaching</h1>
        <h2>Le coaching, qu'est-ce que c'est réellement?</h2>
    </div>
</section>

<section class="coaching coaching__second">
    <div class="contain">
        <h3>Le Coaching, une méthode d'accompagnement personnalisé et efficace</h3>
        <p>Le coaching est une discipline des sciences humaines, une discipline humaniste !</p>
        <p>C’est accompagner une personne ou un collectif à atteindre un objectif personnel. Le coaching permet :</p>
        <ul>
            <li>1. D'apprendre à se connaître</li>
            <li>2. De prendre conscience de ses ressources pour mieux les mobiliser</li>
            <li>3. De lever les obstacles</li>
            <li>4. D'identifier la ou les stratégies possibles</li>
            <li>5. De passer à l'action</li>
            <li>6. D'élargir son champ des possibles</li>
        </ul>
        <p>
            Oui, vous êtes intelligent ! Oui vous possédez un, voire plusieurs talents ! Oui le changement est possible ! Oui les solutions, vos solutions, existent ! Oui vous êtes formidable, parce que nous sommes tous des êtres uniques, riches de notre humanité dotés de ressources qui ne demandent qu’à être exploitées !
        </p>
        <p>
            Oui c’est possible, il suffit d’oser le tout premier petit pas et de passer à l’action en douceur, à votre rythme, en toute sécurité : parlons en !
        </p>


    </div>

</section>

<section class="coaching coaching__third">
    <div class="contain">
        <h3>Vous avez tout pour réussir !</h3>
        <p>Pas de promesse en l’air, la vie idéale et sans effort n’existe que dans les contes, je n’ai (malheureusement) pas de baguette magique, et votre bien-être dépend d’abord de vous mais <b>je peux vous accompagner sur votre chemin</b>, vous aider à <b>reconnaitre les ressources</b> qui sont les vôtres, celles que vous n’osez pas déployer, celles que vous ne voyez pas ou plus, <b>à identifier ce qui vous freine</b> pour lever ou contourner les obstacles, <b>à découvrir que vous avez en vous toutes les clés pour réussir vos projets</b>, à comprendre que la plus belle chose qui puisse vous arriver dans la vie, c’est d’être VOUS, pleinement et sereinement !</p>
        <p>Ce que je vous propose ? De vous mettre en action, un pas après l’autre car <b>l’important c’est d’avancer !</b> Oui ce travail <b>de connaissance de soi, de mise en place de stratégies, d’élaboration d’un plan d’action, de définition de votre objectif...</b> vous pourriez le faire seul, mais s’il est un diction qui resonne à chaque fois que je l’entend, c’est</p>
        <h4>"Seul on va plus vite mais à deux on va plus loin !"</h4>
    </div>
</section>

<section class="coaching coaching__fourth">
    <div class="contain">
        <h3>Ce que le Coaching n'est pas...</h3>
        <ul>
            <li><b>1. Le coaching ce n’est pas du conseil :</b> pas d’apport de solutions externes, les solutions
                viendront de vous, seront les vôtres, adaptées à ce que vous êtes, à votre histoire, votre
                situation…</li>

            <li><b>2. Le coaching n’est pas du mentorat :</b> ce n’est pas le coach qui transmet et investit son
                expérience, c’est vous qui développez votre “sagesse” et votre expertise grâce à un
                questionnement ciblé et stratégique !</li>

            <li><b>3. Le coaching ce n’est pas de la formation :</b> au sens d’un apport externe de connaissances
                préétablies, par contre, vous apprendrez de vous sur vous, c’est certain !</li>

            <li><b>4. Le coaching n’est pas une thérapie :</b> il ne s’agit pas de guérir mais d’accompagner le
                développement et l’évolution pérenne. C’est la réalisation de votre potentiel qui est visé,
                l’affirmation de votre identité et la capacité à vous situer et vous mouvoir dans vos
                relations aux autres et au monde.</li>

            <li><b>5. Le coaching est une démarche auto apprenante,</b> il vise l’évolution ou le
                développement pérenne par la mise en œuvre de ressources intrinsèques !</li>
        </ul>
    </div>
</section>

<section class="coaching coaching__fifth">
    <div class="contain">
        <h3>Le Coaching, est-ce pour moi ?</h3>
        <p>Vous avez envie de <b>redonner du sens</b> à votre vie professionnelle et/ou personnelle ? Vous
            avez envie de <b>retrouver votre motivation ?</b> Vous vous posez des questions qui restent sans
            réponse ? Vous êtes disposé(e) à trouver les réponses grâce à un travail d’introspection et de
            mise en action ? Alors oui, c’est fait pour vous !
        </p>

        <p>
            Le Coaching, c'est trouver <b>ses propres solutions</b>, les plus efficientes possible, <b>pour résoudre sa
                problématique.</b> C’est développer ses performances pour <b>atteindre un objectif personnel et/ou professionnel.</b>
        </p>

        <p>
            C’est se donner l'opportunité de <b>"voir plus loin"</b>, de reconsidérer ses points de vue pour
            mieux avancer.
        </p>
        <a href="/programs.php">Voir les programmes</a>
    </div>
</section>

<?php
require "src/elements/footer.php"
?>

</body>
</html>