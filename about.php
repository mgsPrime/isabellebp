<!DOCTYPE html>
<html lang="fr">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta charset="UTF-8">
    <title>A Propos</title>
    <link rel="stylesheet" href="src/css/global.min.css">
    <!-- Hotjar Tracking Code for https://isabpcoaching.fr -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1659677,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
</head>
<body>
<?php
require "src/elements/header.php"
?>

<section class="about__first" id="first">
    <div class="contain">
        <h1>Isabelle Biarnes-Poulliat</h1>
        <h3>Coach en Développement Personnel et Professionnel</h3>
    </div>
</section>

<section class="about__second" id="second">
    <div class="contain">
        <div class="story">
            <h2>Mon Histoire</h2>
            <div>
                <p>
                    J’ai compris très jeune la valeur et le caractère, à la fois éphémère et exceptionnel, de la vie et de l’humanité dans sa globalité. La vie est courte, elle se mérite, autant en faire une expérience riche de sens et génératrice de bien-être. Cela m’a amenée à orienter mes choix et mes actions dans le sens d’une adéquation entre plaisir et activité. D’abord par l’étude de l’homme dans la société en validant un Master II en Sciences Humaines. Puis en lien avec tout ce qui attrait au monde du travail en me passionnant pour la qualité de vie au travail. Celui-ci occupe, souvent, la majorité de notre temps et je suis convaincue du lien étroit entre bien-être et performance. Mon parcours atypique (d’un BEP Commerce à un BAC + 5 en Psycho socio) ainsi que mes différents engagements bénévoles sont le résultat de cette philosophie.
                </p>
                <p>
                    Douée d’un forte capacité d’écoute, c’est donc naturellement que je me suis orientée vers un métier de Conseillère. Mon ambition était d’amener les jeunes les plus éloignés de l’emploi à s’insérer socialement et professionnellement. Lutter contre le déterminisme social m’a toujours tenu à cœur. Ayant gravi par la suite tous les échelons, ma dernière mission a été d’animer un réseau de directeurs dans une dynamique d’intelligence collective, de mutualisation et de capitalisation afin de faciliter la mission de chacun tout en gagnant en efficacité. Ceci, toujours réalisé au bénéfice des jeunes en difficulté. Organiser la fusion de 3 associations régionales et diriger la nouvelle structure à l’échelle de la Nouvelle-Aquitaine a été pour moi, un challenge, une expérience riche et intéressante qui m’a permis de développer de nouvelles compétences.  Mais malgré ma détermination, le manque de moyens alloués à cette mission m'a fait perdre le sens de mon travail et ma peut-être trop forte conscience professionnelle, m’a amenée à m’y épuiser. Cela a été alors l’occasion, après avoir œuvré 23 ans dans l’accompagnement individuel et collectif dans le domaine associatif, de donner une nouvelle impulsion à ma carrière afin de me lancer dans une activité plus en accord avec mes convictions, ma nature profonde et mon désir d’indépendance.
                </p>
                <p>
                    Mariée, mère de jumelles, c’est lors de cette transition que j’ai découvert à la fois mon hypersensibilité et certaines de mes caractéristiques liées à la dyslexie. De cette connaissance de soi et ayant suivi, la méthode Ikigai, c’est grâce à ce cheminement que j’ai compris mon besoin impérieux d’avoir un impact positif direct sur mon prochain et le fait que j’étais faite pour ça. Je me suis alors formée pendant un an à différents outils d’accompagnement et me suis certifiée en tant que Coach professionnelle.
                </p>
                <p>
                    Ma conviction ? Si chacun trouve sa place, est en accord avec son être et peut exprimer son identité pleine et entière, alors le monde sera meilleur.
                </p>
                <p>
                    Mon moteur ? Accompagner chaque individu à être acteur de sa vie, à donner ou redonner du sens à ses actions, ses projets, pour être en harmonie avec ce qu’il est et avec son environnement ; Amener les individus et les organisations à devenir autonomes dans la mise en place de leurs stratégies de réussite.
                </p>
                <p>
                    Viser l’efficacité, le changement positif et l’épanouissement sont les objectifs qui m’animent dans mon rôle d’accompagnatrice. Pour cela, je m’appuie sur mes valeurs que sont le respect, la bienveillance, la qualité, et l’authenticité pour mener chacune de mes actions avec enthousiasme. A l’image du colibri, qui apporte « sa part » de contribution à un monde meilleur, aussi infime soit elle , c’est lorsque je vois un sourire se dessiner, un regard s’illuminer, un sentiment de réussite naitre chez l’autre que je me sens accomplie.
                </p>
            </div>
        </div>

    </div>
</section>

<section class="about__third" id="third">
    <div class="contain">
        <h2>
            Mes valeurs
        </h2>
        <div class="values">
            <div class="value">
                <h4>Ethique & Professionnalisme</h4>
                <ul>
                    <li>- Je respecte le code de déontologie de l'EMCC (Adhérente)</li>
                    <li>- Je prends du recul et porte un regard critique sur ma pratique</li>
                    <li>- Mes accompagnements sont supervisés</li>
                    <li>- Je me forme continuellement pour un perfectionnement continu et optimum afin d'offrir un accompagnement de qualité</li>
                </ul>
            </div>
            <div class="value">
                <h4>Respect & Bienveillance</h4>
                <ul>
                    <li>- Je respecte mon devoir de confidentialité</li>
                    <li>- Je construis, pour vous, un cadre sécurisé et sécurisant</li>
                    <li>- Liberté d'expression et d'action</li>
                    <li>- Rien ne vous sera jamais imposé mais je serai toujours présente pour vous accompagner dans vos décisions</li>
                </ul>
            </div>
            <div class="value">
                <h4>Enthousiasme & Authenticité</h4>
                <ul>
                    <li>- Accueil chaleureux garanti</li>
                    <li>- Une énergie positive débordante et contagieuse !</li>
                    <li>- Sincérité, et adaptation des techniques à ce que vous êtes</li>
                    <li>- Joie et bonne humeur à chaque instant !</li>
                </ul>
            </div>
            <div class="value">
                <h4>Autonomie & Satisfaction</h4>
                <ul>
                    <li>- Mon objectif est votre autonomie</li>
                    <li>- Je mets en œuvre les moyens nécessaires pour vous permettre d’atteindre vos objectifs</li>
                    <li>- Ma satisfaction sera de voir la vôtre</li>
                    <li>- Le résultat, nous l'aurons construit ensemble, mais c'est surtout vous qui serez à l'origine de cette réussite</li>
                    <li>- Vous pouvez atteindre tous vos objectifs ! Il suffit d'y croire !</li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="about__fourth" id="fourth">
    <div class="contain">
        <h2>
            Ma méthode, ma vision
        </h2>
        <div class="steps">
            <div class="step">
                <div class="number n1">
                    <h3>1</h3>
                </div>
                <h4>Analyse</h4>
                <p>
                    Analyse de votre demande et détermination de votre objectif: la situation que vous désirez atteindre !
                </p>
            </div>
            <div class="step">
                <div class="number n2">
                    <h3>2</h3>
                </div>
                <h4>Exploration</h4>
                <p>
                    Exploration de votre situation actuelle : analyse des motivations et des freins, travail d’introspection, de remises en question...
                </p>
            </div>
            <div class="step">
                <div class="number n3">
                    <h3>3</h3>
                </div>
                <h4>Identification</h4>
                <p>
                    Identification de vos ressources, de vos forces, de vos talents et de ce qui vous motive réellement ! Identification des obstacles présents sur votre chemin pour mettre en place des stratégies de mise en action.
                </p>
            </div>
            <div class="step">
                <div class="number n4">
                    <h3>4</h3>
                </div>
                <h4>Passage à l'action</h4>
                <p>
                    Passage à l’action / Mise en Action.
                </p>
                <p>
                    Vous expérimentez, vous entreprenez, vous agissez, vous osez, vous réussissez !
                </p>
            </div>
        </div>
    </div>
</section>

<?php
require "src/elements/footer.php"
?>

</body>
</html>